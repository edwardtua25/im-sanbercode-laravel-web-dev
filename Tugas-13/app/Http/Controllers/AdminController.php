<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function admin(){
        return view('admin.table');
    }
    public function table(){
        return view('admin.data');
    }
    public function master(){
        return view('admin.master');
    }
    public function navigasi(){
        return view('partial.nav');
    }
    public function sidebar(){
        return view('partial.sidebar');
    }
}
