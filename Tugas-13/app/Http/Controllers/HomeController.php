<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('Home');
    }
    public function data(){
        return view('data');
    }
    public function table(){
        return view('datatables');
    }
}