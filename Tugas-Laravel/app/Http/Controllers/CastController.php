<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast_')->get();
        return view('cast.cast',compact('cast'));
    }
    public function data(){
        return view('cast.data_cast');
    }
    public function form(){
        return view ('cast.cast_create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast_',
            'ummur' => 'required',
            'bio'  => 'required',
        ]);
        $query = DB::table('cast_')->insert([
            "nama" => $request["nama"],
            "ummur" => $request["ummur"],
            "bio"  => $request["bio"],
        ]);
        // dd($request->all());
        return redirect('/cast');
    }
    public function show($id)
    {
        $cast = DB::table('cast_')->where('id', $id)->first();
        return view('cast.show',compact('cast'));
    }
    public function edit($id)
    {
        $cast = DB::table('cast_')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast_',
            'ummur' => 'required',
            'bio'  => 'required',
        ]);

        $query = DB::table('cast_')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'ummur' => $request["ummur"],
                'bio' => $request["bio"]

            ]);
        return redirect('/cast');
    }

    
public function destroy($id)
{
    $query = DB::table('cast_')->where('id', $id)->delete();
    return redirect('/cast');
}
}
