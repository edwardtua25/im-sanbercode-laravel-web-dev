<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class, 'home']);
Route::get('/data-tables',[HomeController::class, 'data']);

Route::get('/register', [AuthController::class, 'register']);

Route::get('/table',[HomeController::class, 'table']);

Route::post('/getnama', [AuthController::class, 'getnama']);
Route::get('/nav',[AdminController::class, 'navigasi']);
Route::get('/side',[AdminController::class, 'sidebar']);

//Route Cast//
Route::GET('/cast',[CastController::class, 'index']);
Route::GET('/data-cast',[CastController::class, 'data']);
Route::GET('/cast/create',[CastController::class, 'form']);
Route::POST('/cast',[CastController::class, 'store']);
Route::GET('/cast/{cast_id}',[CastController::class, 'show']);
Route::GET('/cast/{cast_id}/edit',[CastController::class, 'edit']);
Route::PUT('/cast/{cast_id}',[CastController::class, 'update']);

Route::DELETE('/cast/{cast_id}',[CastController::class, 'destroy']);





