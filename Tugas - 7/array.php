<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <h1>Berlatih Array</h1>

    <?php
    echo "<h3> Soal 1 </h3>";
    /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */ 
        // Lengkapi di sini
    $kids = array("Mike","Dustin","Will","Lucas","Max","Eleven");
    echo ("Kids :".$kids[0].",".$kids[1].",".$kids[2].",".$kids[4].",".$kids[5]);
    echo("<br>");
    $adults = ["Hooper","Nancy","Joyce","Jonathan","Murray"];
    echo ("Adults : " .$adults[0].",".$adults[2].",".$adults[2].",".$adults[3].",".$adults[4]);








    echo "<h3> Soal 2</h3>";
    /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */

    $jumlah_kids = count($kids);
    echo "Cast Stranger Things: ";
    echo "<br>";
    echo "Total Kids: $jumlah_kids"; // Berapa panjang array kids
    echo "<br>";
    echo "<ol>";
    echo "<li> $kids[0] </li>";
    echo "<li> $kids[1] </li>";
    echo "<li> $kids[2] </li>";
    echo "<li> $kids[3] </li>";
    echo "<li> $kids[4] </li>";
    echo "<li> $kids[5] </li>";

    // Lanjutkan

    echo "</ol>";
    $jumlah_adults = count($adults);

    echo "Total Adults: $jumlah_adults"; // Berapa panjang array adults
    echo "<br>";
    echo "<ol>";
    echo "<li> $adults[0] </li>";
    echo "<li> $adults[1] </li>";
    echo "<li> $adults[2] </li>";
    echo "<li> $adults[3] </li>";
    echo "<li> $adults[4] </li>";
    // Lanjutkan

    echo "</ol>";

    echo "<h3> Soal 3</h3>";
    //soal no 3
    $soal3 = array (
        array("Name" =>"Will Byers","Age"=>12,"Aliase"=>"Will the Wise","Status"=>"Alive"),
        array("Name" =>"Mike Wheeler","Age"=>12,"Aliase"=>"Dungeon Master","Status"=>"Alive"),
        array("Name" =>"im Hopper","Age"=>43,"Aliase"=>"Chief Hopper","Status"=>"Deceased"),
        array("Name" =>"Eleven","Age"=>12,"Aliase"=>"El","Status"=>"Alive"),
    );
    echo("<pre>");
    print_r($soal3);
    echo("</pre>");
   
    ?>
</body>

</html>